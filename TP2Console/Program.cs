﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TP2Console.Models.EntityFramework;

namespace TP2Console
{
    class Program
    {
        static void Main(string[] args)
        {
            AddFilms();
        }
        //Afficher tous les films
        public static void Exo2Q1()
        {
            var ctx = new FilmsDBContext();
            foreach (var film in ctx.Film)
            {
                Console.WriteLine(film.ToString());
            }
            Console.ReadKey();
        }
        //Autre possibilité :
        public static void Exo2Q1Bis()
        {
            var ctx = new FilmsDBContext();
            //Pour que cela marche, il faut que la requête envoie les mêmes noms de colonnes

            var films = ctx.Film.FromSqlRaw("SELECT * FROM film");
            foreach (var film in films)
            {
                Console.WriteLine(film.ToString());
            }
            Console.ReadKey();
        }

        //Afficher les emails de tous les utilisateurs
        public static void Exo2Q2()
        {
            var ctx = new FilmsDBContext();
            foreach (var utilisateur in ctx.Utilisateur)
            {
                Console.WriteLine(utilisateur.Email);
            }
            Console.ReadKey();
        }

        //Afficher tous les utilisateurs triés par login croissant
        public static void Exo2Q3()
        {
            var ctx = new FilmsDBContext();
            foreach (var utilisateur in ctx.Utilisateur.OrderBy(el => el.Login))
            {
                Console.WriteLine(utilisateur.ToString());
            }
            Console.ReadKey();
        }
        //Afficher les noms et id des films de la catégorie «Action»
        public static void Exo2Q4()
        {
            var ctx = new FilmsDBContext();
            foreach (var film in ctx.Film.Where(f => f.Categorie == ctx.Categorie.Where(c => c.Nom == "Action").Select(c => c.Id).FirstOrDefault()))
            {
                Console.WriteLine("Id: "+film.Id+" Nom: "+film.Nom);
            }
            Console.ReadKey();
        }

        //Afficher le nombre de catégories
        public static void Exo2Q5()
        {
            var ctx = new FilmsDBContext();
            var nbCat = ctx.Categorie.Count();
            Console.WriteLine("Nombre de catégories: " + nbCat);
            Console.ReadKey();
        }
        //Afficher la note la plus basse dans la base
        public static void Exo2Q6()
        {
            var ctx = new FilmsDBContext();
            var min = ctx.Avis.Min(a => a.Note);
            Console.WriteLine("Note la plus basse: " + min);
            Console.ReadKey();
        }

        //Rechercher tous les films qui commencent par «le» (pas de respect de la casse).
        public static void Exo2Q7()
        {
            var ctx = new FilmsDBContext();
            foreach (var film in ctx.Film.AsEnumerable().Where(f => f.Nom.StartsWith("le", true, null)))
            {
                Console.WriteLine(film.ToString());
            }
            Console.ReadKey();
        }
        //Afficher la note moyenne du film «Pulp Fiction»
        public static void Exo2Q8()
        {
            var ctx = new FilmsDBContext();
            var listeAvis = ctx.Avis.Select(a => a)
                .Include(a => a.FilmNavigation)
                .AsEnumerable()
                .Where(a => a.FilmNavigation.Nom.Equals("pulp fiction", StringComparison.InvariantCultureIgnoreCase));
            var somme = listeAvis.Sum(a => a.Note);
            var count = listeAvis.Count();
            Console.WriteLine("Moyenne de la note du film Pulp Fiction : "+somme/count);
            Console.ReadKey();
        }

        //Afficher l’utilisateur qui a mis la meilleurenote dansla base 
        public static void Exo2Q9()
        {
            var ctx = new FilmsDBContext();
            var utilisateur = ctx.Utilisateur.Select(u => u)
                .Include(u => u.Avis)
                .Where(u => u.Id == ctx.Avis.Max(a => a.Note));
            Console.WriteLine("Utilisateur ayant attribué la meilleur note : ");
            Console.ReadKey();
        }

        //Ajoutez-vous en tant qu’utilisateur
        public static void AddUser()
        {
            var ctx = new FilmsDBContext();
            var user = new Utilisateur()
            {
                Login = "Marine",
                Email = "marine.mollier@cpe.fr",
                Pwd = "123"
            };

            ctx.Utilisateur.Add(user);
            ctx.SaveChanges();
        }

        //Modifier un film
        //Rajouter une description au film «L'armee des douze singes» et le mettre dans la catégorie «Drame»
        public static void UpdateFilm()
        {
            var ctx = new FilmsDBContext();
            var film = ctx.Film.Select(u => u).Where(u => u.Nom == "L'armee des douze singes").FirstOrDefault();
            film.Description = "Description du film";
            film.Categorie = 5;
            ctx.SaveChanges();
        }
        //Supprimer un film et les avis associés
        public static void DeleteFilm()
        {
            var ctx = new FilmsDBContext();
            var film = ctx.Film.Select(u => u).Where(u => u.Nom == "L'armee des douze singes").FirstOrDefault();
            ctx.Film.Remove(film);
            foreach (var avis in ctx.Avis.Where(f => f.Film == 15))
            {
                ctx.Avis.Remove(avis);
            }

            ctx.SaveChanges();
        }

        //Ajouter un avis
        public static void AddAvis()
        {
            var ctx = new FilmsDBContext();
            var avis = new Avis()
            {
                Film = 3,
                Utilisateur = 1,
                Avis1 = "Pas mal",
                Note = 0.61239982200614808M
            };

            ctx.Avis.Add(avis);
            ctx.SaveChanges();
        }

        //Ajouter 2 films dans la catégorie «Drame»
        public static void AddFilms()
        {
            var ctx = new FilmsDBContext();

            IList<Film> films = new List<Film>() {
                                    new Film() { Nom = "Divergente", Description = "Film futuriste", Categorie = 5 },
                                    new Film() { Nom = "Spiderman", Description = "Film de super-heros", Categorie = 5 }
            };
   
            ctx.Film.AddRange(films);
            ctx.SaveChanges();
        }
    }
}
